********************
Exporting a PDF File
********************

The PDF file format is a vector format that can also hold raster images.
Inkscape can save directly to PDF. To do so, do :menuselection:`File --> Save a Copy`,
and select :guilabel:`Portable Document Format (\*.pdf)` from the list of available
file formats at the bottom.

.. Caution::
  Never save as PDF only, but always keep an SVG file, because the
  PDF file format supports a different set of features from the SVG file format
  and you may lose data in the process.

A dialog will then allow you to set the following options:

- :guilabel:`Restrict to PDF version`: Current choices are 1.4 or 1.5. If no specific version is required by the persons who will get the PDF file from you, you can leave it at the default 1.5.
- :guilabel:`Text output options`: If your text needs to searchable in the pdf and the font license allows you to embed the font, choose to :guilabel:`Embed fonts`. When the text is mostly decorative (as in a Logo), it's better to convert the text to paths. If you're writing on a thesis or another scientific document, and know how to work with LaTeX, you can make use of the third option :guilabel:`Omit text in PDF and create LaTeX file`.
- :guilabel:`Rasterize filter effects`: Blurs, shadows and any other filters cannot be used in PDF. This option tells Inkscape to make a 'photo' of any objects/areas in the drawing that have a filter. That way, the drawing will still look the same in a PDF. If you do not select this option, objects that are filtered will look different in the PDF.
- :guilabel:`Resolution for rasterization (dpi)`: The higher the value, the more fine-grained the 'photos' of filtered objects will be. Common values are 96 dpi for screen display, and 300 or 600 dpi for printing.
- :guilabel:`Output page size`: lets you choose which part of the drawing to export to PDF.
- :guilabel:`Bleed/margin`: increases the size of the exported area by adding a margin of the selected width.
- :guilabel:`Limit export to object with ID`: only export the specified object

.. figure:: images/pdf_export_dialog.png
   :alt: PDF export options
   :class: screenshot

   The PDF export dialog offers many options. Usually, the default settings will
   work fine. However, when the pdf is for printing (as opposed to screen display),
   it is recommended that you set the dpi value (resolution) for filtered objects
   to at least 300.

.. admonition:: Tips

  - Inkscape can also open PDF files for editing. Try to avoid multiple round-trips
    through different file formats, though, because each conversion loses data or adds
    unnecessary data.
  - Note that for creating perfect pdf files for printing, it's better to
    turn to a dedicated desktop publishing software such as `Scribus
    <https://www.scribus.net/>`_, which can also import SVG files.
