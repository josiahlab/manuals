******************************
Installing Inkscape on Windows
******************************

Inkscape is available for Microsoft Windows operating systems from XP up to
Windows 10. There are two basic methods for installing Inkscape onto
computers with a Windows operating system. One method is downloading
and installing directly from Inkscape and the other is installing it
from the Microsoft App store. A description of both methods is
presented here. 

.. Attention::
  If you had previously installed Inkscape onto your computer, you will need
  to uninstall that version before installing the new version.

Method 1: Downloading and installing directly from Inkscape
========================================================================

#. Using a Web browser, go to the Inkscape website's `download page for
   Windows <https://inkscape.org/releases/>`_ and select the best download for your system. If you are unsure if you need the 32-bit or 64-bit version, read the section about `Identifying Your System Architecture`_ first.

   .. image:: images/InkScape4.png
      :class: screenshot

#. Select an installation method from the available choices (:term:`exe <Executable File (.exe)>`, :term:`msi
   <Windows Installer Package Files (.msi)>`, :term:`portable <Portable App>`) and wait for it to
   download. You should either see a window giving you the option to save
   the file, or a pop-up appear in your web browser with
   the file’s name and a timer stating how long until the download is
   complete.

   .. image:: images/InkScape5.png
      :class: screenshot

#. Once the download is complete, either click on the file in the lower
   left corner of your screen to start the installation process or, if need
   be, go to your file explorer, open your downloads folder and select
   the file from there. It should be the first file at the top of the
   folder.

   .. image:: images/InkScape6.png
      :class: screenshot

   **OR**

   .. image:: images/InkScape7.png
      :class: screenshot

   .. image:: images/InkScape8.png
      :class: screenshot

   .. image:: images/InkScape9.png
      :class: screenshot

#. If you get a :guilabel:`User Account Control` pop-up from Windows similar
   to the following one click “OK” and wait for the Inkscape installation program to start.

   .. image:: images/InkScape10.2.png
      :class: screenshot

#. Select what language you want to use during the installation and
   click :guilabel:`OK`. Then click :guilabel:`Next` on both the following
   welcome screen and license agreement screen.

   .. image:: images/InkScape11.png
      :class: screenshot

#. On the :guilabel:`Choose Components` screen you can select which features
   you want to install or not install. In most cases the default options should provide all that the user needs, so click :guilabel:`Next`.  

   .. image:: images/InkScape12.png
      :class: screenshot

#. On the :guilabel:`Choose Install Location` window leave the destination
   folder as :file:`C:\\Program Files\\Inkscape` and click
   :guilabel:`Install` unless you want Inkscape to be installed in a
   specific location on your computer.

   .. image:: images/InkScape13.png
      :class: screenshot

   (Optional step)

   If you do want to install Inkscape in a specific location on your
   computer, click the :guilabel:`Browse` button and in the resulting window
   either make a new folder for Inkscape and select it or select the
   destination folder you want Inkscape to be installed to.

   .. image:: images/InkScape14.png
      :class: screenshot

#. Once you click :guilabel:`Install`, a progress bar will appear showing
   how long it will take for your program to install.

#. After the installation is complete, click :guilabel:`Finish` and
   the installer will automatically open Inkscape for you. You are
   ready to begin working with Inkscape.

   .. image:: images/InkScape15.png
      :class: screenshot

Method 2: Installing from the Microsoft App store
==============================================================

You can also install Inkscape onto your Windows computer from the
Microsoft App store. If you have the Windows 10 Education version this
will be the only way you can install Inkscape.

.. Note::
  You will need a Microsoft account to install apps from the Microsoft store. If you already have an account, skip the following steps on how to make one.

#. To create a Microsoft account using a Web browser go to
   `https://login.live.com/ <https://login.live.com/>`_
   and select :guilabel:`Create one!`.

   .. image:: images/InkScape16.png
      :class: screenshot

#. On the following page enter in the email address or phone number that
   you would like to link your Microsoft Account to and then click :guilabel:`Next`.
   There is also the option to get a new email address from Microsoft.

   .. Note::
     We will be using a preexisting email for this example but if you
     want to create a new email the process for making a Microsoft email is
     pretty straight forward.

   .. image:: images/InkScape17.png
      :class: screenshot

#. Enter a password that you wish to use with your new Microsoft account
   and click :guilabel:`Next`.

   .. image:: images/InkScape18.png
      :class: screenshot

#. Enter in your first and last name then click :guilabel:`Next`.

   .. image:: images/InkScape19.png
      :class: screenshot

#. Enter the country/region in which you live and your birthdate.

   .. image:: images/InkScape20.png
      :class: screenshot

#. Check the email you used to create the account and find the email
   from Microsoft. Enter the code from the email into the web form and click
   :guilabel:`Next`.

   .. image:: images/InkScape21.png
      :class: screenshot

#. Enter your phone number and then click :guilabel:`send code` to receive
   the security code, then click :guilabel:`Next`.

   .. image:: images/InkScape22.png
      :class: screenshot

#. On the following page verify that the information is correct and
   click :guilabel:`Looks good!` and your Microsoft Account is ready for use.

   .. image:: images/InkScape23.png
      :class: screenshot

#. From the task bar at the bottom of your screen click the Microsoft
   Store icon or enter 'store' into the search bar and select 'Microsoft
   Store' from the results.

   .. image:: images/InkScape24.png
      :class: screenshot

   **OR**

   .. image:: images/InkScape25.png
      :class: screenshot

#. Enter 'Inkscape' into the Microsoft Store search bar and select
   'Inkscape' from the results.

   .. image:: images/InkScape26.png
      :class: screenshot

#. In the following windows to install Inkscape click :guilabel:`Get` and
   then enter your Microsoft account username and then password in the
   window that pops up.

   .. image:: images/InkScape27.png
      :class: screenshot

   .. image:: images/InkScape28.png
      :class: screenshot

   .. image:: images/InkScape4.png
      :class: screenshot

#. After signing into your Microsoft Account click :guilabel:`Install` and
   the installation will begin.

   .. image:: images/InkScape30.png
      :class: screenshot

#. Once the download and installation is complete click :guilabel:`Launch`
   and you are ready to begin working with Inkscape.

   .. image:: images/InkScape31.png
      :class: screenshot

Congratulations! You have now installed Inkscape onto your Windows
computer. 

If you need further help, please visit `the FAQ section for Windows
specific problems <https://inkscape.ort/learn/faq/#windows-specific-issues>`_ on the Inkscape website.

Identifying Your System Architecture
====================================

Before you can begin to download Inkscape, you must first know what type
of operating system (OS) you have, 32-bit or 64-bit. If you already know
what type of operating system you have please skip this section and go
the section on downloading Inkscape.

Method 1: Using Keyboard Shortcuts
==================================

#. Press :kbd:`Win` + :kbd:`r` to open the Windows Run dialog.

#. Type "msinfo32" and press :kbd:`Enter`.

#. Now in the new dialog, toward the middle of the screen, you should see an
   option that says :guilabel:`System Type`. Write down the associated information.

   You will need this to select the correct Inkscape download file for your
   system.

   .. image:: images/install_win_msinfo32.png
      :class: screenshot   

Method 2: Using the mouse
=========================

#. Click on the magnifying glass to open a search dialog and type
   'system'.

   .. image:: images/install_win_start_sys.png
      :class: screenshot

#. Click to open the :guilabel:`System` control panel.

#. Depending on the version of MS Windows on your computer, you may see one of the dialogs shown below. Now on that dialog, towards the middle of the screen, you should see an
   option that says :guilabel:`System Type`. Write down the associated information.

   You will need this to select the correct Inkscape download file for your
   system.

   .. image:: images/install_win_system.png
      :class: screenshot
